public class Input {
    private String value;
    private int count;

    public Input(String word, int input){
        this.value = word;
        this.count = input;
    }


    public String getValue() {
        return this.value;
    }

    public int getWordCount() {
        return this.count;
    }


}
