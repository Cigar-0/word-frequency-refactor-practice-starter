import java.util.*;


public class WordFrequencyGame {

    public static final String SPLIT_BREAK = "\\s+";
    public static final int INIT_NUM = 1;

    public String getResult(String inputStr) {

        try {

            //split the input string with 1 to n pieces of spaces
            List<Input> inputList = transformToInput(inputStr);

            //get the map for the next step of sizing the same word
            inputList = countInputFrequency(inputList);
            inputList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
            return formatReport(inputList);

        } catch (Exception e) {

            return "Calculate Error";
        }
    }


    private static String formatReport(List<Input> inputList) {
        StringJoiner joiner = new StringJoiner("\n");
        inputList.forEach(input -> joiner.add(input.getValue() + " " + input.getWordCount()));
        return joiner.toString();
    }

    private static List<Input> countInputFrequency(List<Input> inputList) {

        Map<String, List<Input>> map = new HashMap<>();
        inputList.forEach(input -> {
            if (!map.containsKey(input.getValue())) {
                ArrayList valueList = new ArrayList<>();
                valueList.add(input);
                map.put(input.getValue(), valueList);
            } else {
                map.get(input.getValue()).add(input);
            }
        });

        List<Input> inputCountList = new ArrayList<>();
        map.forEach((key, value) -> inputCountList.add(new Input(key, value.size())));
        return inputCountList;

    }

    private static List<Input> transformToInput(String inputStr) {
        List<Input> inputList = new ArrayList<>();
        Arrays.stream(inputStr.split(SPLIT_BREAK)).forEach(input -> inputList.add(new Input(input, INIT_NUM)));
        return inputList;
    }


}
