## ORID

### O：

Today, we first conducted a code review. For weekends homework, I found the team members' good ideas to solve the problem. Then we did presentation about Design pattern. My team topic is Strategy pattern. I'm mainly responsible for explaining why you should use the strategy pattern. The group work went more smoothly than last time. 

  Then we learned the concept of refactor. Learned how to refactor code, when to refactor code, and common code smells. The process of finding code smell is difficult and easy to miss. Code refactoring makes the developed code more standardized and easier to maintain. 

### R：

My team topic is Strategy pattern. I'm mainly responsible for explaining why you should use the strategy pattern. The group work went more smoothly than last time, perhaps because everyone was familiar with it. 

### I：

Today, while searching for smells in the code, I also discovered some of the smells I used to use frequently, such as mysteries smell and loop smell.Through Code refactoring, I also have a new direction for standardizing code.

### D:

In the future development, I also want to develop the habit of code refactoring.

